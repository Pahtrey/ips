<?php
    function last($str = "")
    {
        return substr($str, strlen($str) - 1);
    }

    function withoutLast($str = "")
    {
        return substr($str, 0, strlen($str) - 1);
    }

    function reverseStr($str = "")
    {
        $len = strlen($str) - 1;
        $resultStr = "";
        for($i = $len; $i >= 0; $i--)
        {
            $resultStr .= $str[$i];
        }
        return $resultStr;
    }